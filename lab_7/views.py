from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper(os.environ.get("SSO_USERNAME", "username"),
                         os.environ.get("SSO_PASSWORD", "cGFzc3dvcmQ="))


def index(request):
    mahasiswa_list = csui_helper.get_mahasiswa_list()

    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)


def next_page(request):
    if getattr(csui_helper, 'next_page', None) is None:
        mahasiswa_list = csui_helper.get_mahasiswa_list()
    else:
        mahasiswa_list = csui_helper.next_mahasiswa_list()

    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)


def previous_page(request):
    if getattr(csui_helper, 'previous_page', None) is None:
        mahasiswa_list = csui_helper.get_mahasiswa_list()
    else:
        mahasiswa_list = csui_helper.previous_mahasiswa_list()

    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)


def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)


def friend_list_json(request):
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({"results": friends})


@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        if Friend.objects.filter(npm=npm).exists():
            return HttpResponse({'message': 'Already exists'}, status=400)
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data)


def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponseRedirect('/lab-7/get-friend-list/')


@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)


def model_to_dict(obj):
    data = serializers.serialize('json', [obj, ])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data
