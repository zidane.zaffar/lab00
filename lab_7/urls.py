from django.conf.urls import url
from .views import (
    index,
    add_friend,
    validate_npm,
    delete_friend,
    friend_list,
    friend_list_json,
    next_page,
    previous_page,
)

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-friend/$', add_friend, name='add-friend'),
    url(r'^validate-npm/$', validate_npm, name='validate-npm'),
    url(r'^delete-friend/(?P<friend_id>[0-9]+)/$',
        delete_friend, name='delete-friend'),
    url(r'^get-friend-list-json/$',
        friend_list_json, name='get-friend-list-json'),
    url(r'^get-friend-list/$', friend_list, name='get-friend-list'),
    url(r'^get-friend-list/next-page/$',
        next_page, name='get-friend-list-next'),
    url(r'^get-friend-list/previous-page/$',
        previous_page, name='get-friend-list-previous'),
]
